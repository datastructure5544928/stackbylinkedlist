/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package stackbylinkedlist;

/**
 *
 * @author informatics
 */
public class Link {

    public long dData; // data item
    public Link next; // next link in list

    public Link(long dd) // constructor
    {
        dData = dd;
    }

    public void displayLink() // display ourself
    {
        System.out.print(dData + " "); 
    }
}
