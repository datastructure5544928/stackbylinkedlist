/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package stackbylinkedlist;

/**
 *
 * @author informatics
 */
public class LinkStack {

    private LinkList theList;
//--------------------------------------------------------------

    public LinkStack() // constructor
    {
        theList = new LinkList();
    }
//--------------------------------------------------------------

    public void push(long j) // put item on top of stack
    {
        theList.insertFirst(j);
    }
//--------------------------------------------------------------

    public long pop() // take item from top of stack
    {
        return theList.deleteFirst();
    }
//--------------------------------------------------------------

    public boolean isEmpty() // true if stack is empty
    {
        return (theList.isEmpty());
    }
//--------------------------------------------------------------

    public void displayStack() {
        System.out.print("Stack(top-- > bottom): ");
        theList.displayList();
    }

}
